let id = 1;
let user = {
    password: "password",
    login: "login",
};

const fs = require('fs');
let fileUsers = fs.readFileSync("data.json", "utf-8");
let users = [fileUsers];

const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());

app.get('/users', (req, res) => {
    res.json(users);
});

app.get('/users/:id', (req, res) =>{
    res.json(users.find(user => user.id === +res.params.id) || {message: 'Пользователь найден!'});
})

app.post('/users', (req, res) => {
    console.log(req.body);
    if(!req.body){
        return res.json({message: "Вы забыли передать тело запроса"});
    }
    if(!req.body.password || req.body.password < 6){
        res.json({message: "Пароль сликом короткий!"});
    }
    if(!req.body.login || req.body.login < 6 ){
        res.json({message: "Логин сликом короткий!"});
    }
    let  newUser = {
        password: req.body.password,
        login: req.body.login,
        id: id++
    }
    users.push(newUser);
    res.json(newUser);
});

app.delete('/users/:id', (req, res) => {
    const userId = +req.params.id
    if(isNaN(userId)) {
        return res.json({message: 'Вы передали неверный id'});
    }
    let index = users.findIndex(user => user.id === userId);
    if(index === 1){
        return res.json({message: 'Вы передали неверный id, пользователь не найден!'});
    }
    users.splice(index, 1);
    res.json({message: "Пользователь успешно удален!"});
});

app.use( (req, res) =>{
    console.log("Url: " + req.url);
    console.log("Тип запроса: " + req.method);
    res.json({messsage: "Данный Url адрес не обрабатывается!"});
});

app.listen(3000);